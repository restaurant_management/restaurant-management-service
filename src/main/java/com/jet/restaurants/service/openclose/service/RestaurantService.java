package com.jet.restaurants.service.openclose.service;

import com.jet.restaurants.service.openclose.domain.Restaurant;
import com.jet.restaurants.service.openclose.domain.Status;
import com.jet.restaurants.service.openclose.repo.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class RestaurantService {
    private final RestaurantRepository restaurantRepository;

    private final KafkaTemplate<String, String> kafkaTemplate;

    @Value("${topic.name.consumer}")
    private String topicName;

    @Autowired
    public RestaurantService(RestaurantRepository restaurantRepository, KafkaTemplate<String, String> kafkaTemplate) {
        this.restaurantRepository = restaurantRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    public Restaurant createRestaurant(String name, Status status) {

        return restaurantRepository.save(Restaurant.create(name, status));
    }

    public Restaurant updateRestaurantStatus(Integer restaurantId, Status status) {

        Restaurant restaurant = verifyRestaurant(restaurantId);
        restaurant.setStatus(status);
        restaurantRepository.save(restaurant);

        kafkaTemplate.send(topicName, restaurant.getName(), status.getStatus());

        return restaurant;
    }

    private Restaurant verifyRestaurant(Integer restaurantId) throws NoSuchElementException {
        return restaurantRepository.findById(restaurantId).orElseThrow(() ->
                new NoSuchElementException("Restaurant not found with id:" + restaurantId));
    }

}
